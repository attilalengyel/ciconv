# CIConv

Color invariant convolutions for "Zero-Shot Domain Adaptation with a Physics Prior".

This repository is used to store the pre-trained model weights, no need to clone locally. Use https://github.com/Attila94/CIConv instead.
